---
layout: 'page'
title: 'Terima Kasih'
permalink: '/donate/'
toc: true
---

<img class="post-body-img" src="{{ site.lazyload.logo_blank_banner }}" data-echo="/assets/img/banner/banner.png" alt="banner">

Terima kasih kepada teman-teman yang telah mengapresiasi tulisan-tulisan di bandithijo.dev dengan berdonasi ke {% link saweria/bandithijo | https://saweria.co/bandithijo %}.

Semoga sebagian rizqi yang teman-teman berikan, Allah lipat gandakan dengan yang lebih baik.

# Honor for Donor

1. **Uddin** (23/08/2019 via. GoPay)
1. **thebugwal** (06/27/2022 via. Saweria) \
   Semangat terus bikin kontennya, Bang. Ane banyak belajar dari blog ente. Keep Spirit!!
1. **zxce3** (03/12/2024 via. Saweria) \
   Keep up the good work!
