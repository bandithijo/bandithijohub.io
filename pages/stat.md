---
layout: 'page'
title: 'Coding Stat'
permalink: '/stat/'
author: 'BanditHijo'
toc: true
---

## Coding activity over time

[![wakatime](https://wakatime.com/badge/user/1e45775c-2d2f-46db-9f24-f0724d2323ff.svg)](https://wakatime.com/@1e45775c-2d2f-46db-9f24-f0724d2323ff)


## Coding activity over last 7 days

<figure><embed src="https://wakatime.com/share/@bandithijo/18d89296-ef1a-4f59-aee4-86317689ad66.svg"/></figure>


## Languages over last 7 days

<figure><embed src="https://wakatime.com/share/@bandithijo/bfd94d66-9260-4b7e-b232-878433ea2834.svg"/></figure>


## Editors over last 7 days

<figure><embed src="https://wakatime.com/share/@bandithijo/b418a692-abf0-4b88-a43f-d97e8d981a39.svg"/></figure>


## Operating Systems over last 7 days

<figure><embed src="https://wakatime.com/share/@bandithijo/c9c72fbf-09da-481a-b0dd-8b831c341937.svg"/></figure>

